package com.rohansakhale.sorting;

public interface ISorting<T> {

	public T[] sort(T[] arr) throws Exception;

	public void print(T[] arr);
}
