/**
 * 
 */
package com.rohansakhale.sorting.impl;

import java.util.logging.Logger;

import com.rohansakhale.sorting.ISorting;

/**
 * @author Rohan Sakhale
 *
 */
public class BubbleSort implements ISorting<Integer> {

	private static Logger LOG = Logger.getLogger(BubbleSort.class.getName());

	public Integer[] sort(Integer[] arr) {
		int temp;
		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] > arr[j]) {
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		return arr;
	}

	public void print(Integer[] arr) {
		for (int i = 0; i < arr.length; i++) {
			LOG.info("arr[" + i + "] => " + arr[i]);
		}
	}

}
