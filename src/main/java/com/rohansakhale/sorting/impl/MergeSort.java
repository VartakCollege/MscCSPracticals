package com.rohansakhale.sorting.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rohansakhale.sorting.ISorting;

public class MergeSort implements ISorting<Integer> {

	private Integer[] arr;
	private Integer[] mergeArr;
	private int length;

	private static Logger LOG = LoggerFactory.getLogger(MergeSort.class);

	@Override
	public Integer[] sort(Integer[] arr) {
		this.arr = arr;
		this.length = arr.length;
		this.mergeArr = new Integer[this.length];
		doMergeSort(0, this.length - 1);
		return arr;
	}

	private void doMergeSort(int lowerIndex, int higherIndex) {
		if (higherIndex > lowerIndex) {
			int middleIndex = lowerIndex + (higherIndex - lowerIndex) / 2;

			// sort the left array
			doMergeSort(lowerIndex, middleIndex);

			// sort the right array
			doMergeSort(middleIndex + 1, higherIndex);

			// merge parts of the arrays
			mergeParts(lowerIndex, middleIndex, higherIndex);
		}
	}

	private void mergeParts(int lowerIndex, int middleIndex, int higherIndex) {
		// copy array into temp array
		for (int i = lowerIndex; i <= higherIndex; i++) {
			mergeArr[i] = arr[i];
		}
		int i = lowerIndex;
		int j = middleIndex + 1;
		int k = lowerIndex;
		while (i <= middleIndex && j <= higherIndex) {
			if (mergeArr[i] <= mergeArr[j]) {
				arr[k] = mergeArr[i];
				i++;
			} else {
				arr[k] = mergeArr[j];
				j++;
			}
			k++;
		}
		while (i <= middleIndex) {
			arr[k] = mergeArr[i];
			k++;
			i++;
		}
	}

	@Override
	public void print(Integer[] arr) {
		for (int i = 0; i < arr.length; i++) {
			LOG.info("arr[" + i + "] => " + arr[i]);
		}
	}

}
