package com.rohansakhale.sorting.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rohansakhale.sorting.ISorting;

public class PermuteBySort implements ISorting<Integer> {

	private static Logger LOG = LoggerFactory.getLogger(PermuteBySort.class);

	private Integer[] values;
	private Integer[] mergeValues;
	private Integer[] priorities;
	private Integer[] mergePriorities;

	@Override
	public Integer[] sort(Integer[] arr) throws Exception {
		throw new Exception("Method not allowed");
	}

	private void doMergeSort(int lowerIndex, int higherIndex) {
		if (higherIndex > lowerIndex) {
			int middleIndex = lowerIndex + (higherIndex - lowerIndex) / 2;

			// sort the left array
			doMergeSort(lowerIndex, middleIndex);

			// sort the right array
			doMergeSort(middleIndex + 1, higherIndex);

			// merge parts of the arrays
			mergeParts(lowerIndex, middleIndex, higherIndex);
		}
	}

	private void mergeParts(int lowerIndex, int middleIndex, int higherIndex) {
		// copy array into temp array
		for (int i = lowerIndex; i <= higherIndex; i++) {
			mergePriorities[i] = priorities[i];
			mergeValues[i] = values[i];
		}
		int i = lowerIndex;
		int j = middleIndex + 1;
		int k = lowerIndex;
		while (i <= middleIndex && j <= higherIndex) {
			if (mergePriorities[i] <= mergePriorities[j]) {
				priorities[k] = mergePriorities[i];
				values[k] = mergeValues[i];
				i++;
			} else {
				priorities[k] = mergePriorities[j];
				values[k] = mergeValues[j];
				j++;
			}
			k++;
		}
		while (i <= middleIndex) {
			priorities[k] = mergePriorities[i];
			values[k] = mergeValues[i];
			k++;
			i++;
		}
	}

	public List<Integer[]> sort(Integer[] values, Integer[] priority) {
		List<Integer[]> sorted = new ArrayList<>();

		// Make use of Merge Sort for faster sort
		this.values = values;
		this.priorities = priority;

		this.mergePriorities = new Integer[values.length];
		this.mergeValues = new Integer[values.length];

		// Start Merge Sort
		doMergeSort(0, values.length - 1);

		sorted.add(values);
		sorted.add(priority);
		return sorted;
	}

	@Override
	public void print(Integer[] arr) {
		// Not defined
	}

	/**
	 * Print value and their priorities
	 * 
	 * @param values
	 *            Integer array of values
	 * @param priority
	 *            Integer array of priorities
	 */
	public void print(Integer[] values, Integer[] priority) {
		for (int i = 0; i < values.length; i++) {
			LOG.info("Values[" + i + "] = " + values[i]);
			LOG.info("Priority[" + i + "]" + priority[i]);
		}
	}

}
