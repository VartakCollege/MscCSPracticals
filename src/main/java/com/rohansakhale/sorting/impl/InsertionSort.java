package com.rohansakhale.sorting.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rohansakhale.sorting.ISorting;

public class InsertionSort implements ISorting<Integer> {

	private static Logger LOG = LoggerFactory.getLogger(InsertionSort.class);

	public InsertionSort() {
	}

	@Override
	public Integer[] sort(Integer[] arr) {
		for (int i = 0; i < arr.length; i++) {
			int value = arr[i];
			int j = i - 1;
			while ((j > -1) && (arr[j] > value)) {
				arr[j + 1] = arr[j];
				j--;
			}
			arr[j + 1] = value;
			print(arr);
		}
		return arr;
	}

	@Override

	public void print(Integer[] arr) {
		for (int i = 0; i < arr.length; i++) {
			LOG.info("arr[" + i + "] => " + arr[i]);
		}
	}

}
