package com.rohansakhale.sorting.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rohansakhale.sorting.ISorting;
import com.rohansakhale.sorting.impl.InsertionSort;

public class InsertionSortTest {

	Random random;
	Integer[] arr;
	ISorting<Integer> insertionSort;
	int testLimit = 1000;
	private static Logger LOG = LoggerFactory.getLogger(InsertionSortTest.class);

	@Before
	public void setUp() throws Exception {
		LOG.info("Setting up array");
		random = new Random();
		insertionSort = new InsertionSort();
		arr = new Integer[testLimit];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = random.nextInt(testLimit * 10);
		}
		LOG.info("Array initialized");
		insertionSort.print(arr);
	}

	@Test
	public void test() {
		Boolean isValid = true;
		try {
			arr = insertionSort.sort(arr);
			LOG.info("Array after sort");
			insertionSort.print(arr);

			for (int i = 0; i < arr.length - 1; i++) {
				if (arr[i] > arr[i + 1]) {
					fail("Failed as array is not sorted properly");
					isValid = false;
				}
			}
		} catch (Exception e) {
			isValid = false;
			e.printStackTrace();
		}
		assertTrue(isValid);
	}

}
