package com.rohansakhale.sorting.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rohansakhale.sorting.ISorting;
import com.rohansakhale.sorting.impl.MergeSort;

public class MergeSortTest {

	Random random;
	Integer[] arr;
	ISorting<Integer> mergeSort;
	int testLimit = 1000;
	private static Logger LOG = LoggerFactory.getLogger(InsertionSortTest.class);

	@Before
	public void setUp() throws Exception {
		LOG.info("Setting up array");
		random = new Random();
		mergeSort = new MergeSort();
		arr = new Integer[testLimit];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = random.nextInt(testLimit * 10);
		}
		LOG.info("Array initialized");
		mergeSort.print(arr);
	}

	@Test
	public void test() {
		Boolean isValid = true;
		try {
			arr = mergeSort.sort(arr);
			LOG.info("Array after sort");
			mergeSort.print(arr);

			for (int i = 0; i < arr.length - 1; i++) {
				if (arr[i] > arr[i + 1]) {
					fail("Failed as array is not sorted properly");
					isValid = false;
				}
			}
		} catch (Exception e) {
			isValid = false;
			e.printStackTrace();
		}
		assertTrue(isValid);
	}

}
