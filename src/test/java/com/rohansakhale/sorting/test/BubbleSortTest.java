package com.rohansakhale.sorting.test;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rohansakhale.sorting.ISorting;
import com.rohansakhale.sorting.impl.BubbleSort;

public class BubbleSortTest {

	Random random;
	Integer[] arr;
	ISorting<Integer> bubbleSort;
	int testLimit = 1000;
	private static Logger LOG = LoggerFactory.getLogger(BubbleSortTest.class);

	@Before
	public void setUp() throws Exception {
		LOG.info("Setting up array");
		random = new Random();
		bubbleSort = new BubbleSort();
		arr = new Integer[testLimit];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = random.nextInt(testLimit * 10);
		}
		LOG.info("Array initialized");
		bubbleSort.print(arr);
	}

	@Test
	public void testBubbleSort() {
		Boolean isValid = true;
		try {
			arr = bubbleSort.sort(arr);
			LOG.info("Array after sort");
			bubbleSort.print(arr);
			for (int i = 0; i < arr.length - 1; i++) {
				if (arr[i] > arr[i + 1]) {
					fail("Failed as array is not sorted properly");
					isValid = false;
				}
			}
		} catch (Exception e) {
			isValid = false;
			e.printStackTrace();
		}

		assertTrue(isValid);
	}

}
