package com.rohansakhale.sorting.test;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import com.rohansakhale.sorting.impl.PermuteBySort;

public class PermuteBySortTest {

	PermuteBySort permuteBySort;
	Integer[] values;
	Integer[] priorities;
	Random random;

	int indexLimit = 1000;

	@Before
	public void setUp() throws Exception {
		permuteBySort = new PermuteBySort();

		random = new Random();
		values = new Integer[indexLimit];
		priorities = new Integer[indexLimit];
		Integer upperLimit = indexLimit * indexLimit * indexLimit;
		for (int i = 0; i < indexLimit; i++) {
			values[i] = random.nextInt(upperLimit);
			priorities[i] = random.nextInt(indexLimit);
		}
	}

	@Test
	public void test() {
		permuteBySort.sort(values, priorities);
		assertTrue(true);
	}

}
