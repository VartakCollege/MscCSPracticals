# M.Sc. Computer Science Practicals

[![build status](https://gitlab.com/VartakCollege/MscCSPracticals/badges/master/build.svg)](https://gitlab.com/VartakCollege/MscCSPracticals/commits/master)
[![coverage report](https://gitlab.com/VartakCollege/MscCSPracticals/badges/master/coverage.svg)](https://gitlab.com/VartakCollege/MscCSPracticals/commits/master)

Various practicals performed in Java would be shared here by the students of 
Vartak College.